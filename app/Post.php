<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Post;


class Post extends Model
{
    protected $table = 'posts';

    protected $fillable = ['title', 'description', 'cover_image', 'featured', 'user_id'];

    public $primaryKey = 'id';

    public $timestamps = true;

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function categories(){
    	return $this->hasMany('App\Category');
    }
}
