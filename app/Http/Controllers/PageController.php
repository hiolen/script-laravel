<?php

    namespace App\Http\Controllers;

    class PageController extends Controller
    {
        
        /**
         * Return the homepage view
         * @return mixed
         */   
        public function home()
        {
            return view('pages.index');

        } 

        /**
         * Return the javascript view
         * @return mixed
         */   
        public function javascript()
        {
            return view('pages.javascript');

        }         

        /**
         * Return the nodejs view
         * @return mixed
         */   
        public function nodejs()
        {
            return view('pages.nodejs');

        }

        /**
         * Return the angular view
         * @return mixed
         */   
        public function angular()
        {
            return view('pages.angular');

        }         

        /**
         * Return the react view
         * @return mixed
         */   
        public function react()
        {
            return view('pages.react');

        }         

        /**
         * Return the api dashboard view listing available apis
         * @return mixed
         */ 
        public function api()
        {
            return view('apidashboard');

        }    

        /**
         * Return the contact page view
         * @return mixed
         */ 
        public function contact()
        {
            return view('contact');

        }
    }
