<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Post;

class DashboardController extends Controller
{
	public function __construct(){
		$this->middleware('auth');
	}

    public function index(){
    	$posts = Post::orderBy('created_at', 'desc')->get();

    	return view('dashboard.index')->with('posts', $posts);
    }
}
