<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\ResponseFactory;

use App\Http\Requests;

use App\Post;
use App\User;
use Auth;

class PostController extends Controller
{
    public function __construct(ResponseFactory $response){
        $this->response = $response;
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    public function postList(){
        $users = User::pluck('fullname', 'id');
        
        return view('dashboard.post.index')->with('users', $users);
    }

    public function postRead(){
        $posts = Post::orderby('created_at', 'desc')->get();

        return view('dashboard.layouts.partials.postList')->with('posts', $posts);
    }

    public function postAdd(Request $request){
        if ($request->ajax()){
            // $post = Post::create($request->all());
            if ($request->hasFile('cover_image')){
                $filenameWithExt =  $request->file('cover_image')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $request->file('cover_image')->getClientOriginalExtension();
                $fileNameToStore = $filename.'_'.time().'.'.$extension;
                $path = $request->file('cover_image')->move(public_path().'/cover_image', $fileNameToStore);
            }else {
                $fileNameToStore = 'noimage.jpg';
            }

            $post = new Post;
            $post->title = $request->input('title');
            $post->description = $request->input('description');
            $post->cover_image = $fileNameToStore;
            $post->featured = $request->input('featured');
            $post->user_id = $request->input('user_id');
            $post->save();
            
            return response($post);
        }
    }

    public function postEdit(Request $request){
        if ($request->ajax()){
            $post = Post::find($request->id);
            
            return $this->response->json($post);
        }   
    }

    public function postUpdate(Request $request){
        if ($request->ajax()){
            if ($request->hasFile('cover_image')){
                $filenameWithExt =  $request->file('cover_image')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $request->file('cover_image')->getClientOriginalExtension();
                $fileNameToStore = $filename.'_'.time().'.'.$extension;
                $path = $request->file('cover_image')->move(public_path().'/cover_image', $fileNameToStore);
            }else {
                $fileNameToStore = 'noimage.jpg';
            }

            $post = Post::find($request->id);
            $post->title = $request->input('title');
            $post->description = $request->input('description');
            $post->cover_image = $fileNameToStore;
            $post->featured = $request->input('featured');
            $post->user_id = $request->input('user_id');
            $post->save();
            
            return response($post);
        }   
    }


    public function postDestroy(Request $request){
        if ($request->ajax()){
            Post::destroy($request->id);

            return response(['message'=>'Post deleted successfully!']);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where('featured', 0)->orderby('created_at', 'desc')->paginate(5);

        $featured = Post::where('featured', 1)->first();

        return view('posts.index', compact('posts', 'featured'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'title' => 'required | max:150',
                'description' => 'required',
                'cover_image' => 'required|image|mimes:jpeg,jpg,png,gif,svg|max:2048'
            ]);

        if ($request->hasFile('cover_image')){
            $filenameWithExt =  $request->file('cover_image')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            $path = $request->file('cover_image')->move(public_path().'/cover_image', $fileNameToStore);
        }else {
            $fileNameToStore = 'noimage.jpg';
        }

        // Create Post
        $post = new Post;
        $post->title = $request->input('title');
        $post->description = $request->input('description');
        $post->user_id = Auth::user()->id;
        $post->cover_image = $fileNameToStore;
        $post->save();

        return redirect('/post')->with('success', 'Post Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);

        return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        if (Auth::user()->id !== $post->user->id){
            return redirect('/post')->with('error', 'Unathorized Page!');
        }

        return view('posts.edit')->with('post', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
                'title' => 'required | max:150',
                'description' => 'required',
                'cover_image' => 'required|image|mimes:jpeg,jpg,png,gif,svg|max:2048'
            ]);

        if ($request->hasFile('cover_image')){
            $filenameWithExt =  $request->file('cover_image')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            $path = $request->file('cover_image')->move(public_path().'/cover_image', $fileNameToStore);
        }else {
            $fileNameToStore = 'noimage.jpg';
        }


        // Create Post
        $post = Post::find($id);
        $post->title = $request->input('title');
        $post->description = $request->input('description');
        $post->user_id = Auth::user()->id;
        $post->cover_image = $fileNameToStore; 
        $post->save();

        return redirect('/post')->with('success', 'Post Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        if (Auth::user()->id !== $post->user->id){
            return redirect('/post')->with('error', 'Unathorized Page!');
        }

        $post->delete();

        return redirect('/post')->with('success', 'Post Remove');
    }
}
