@extends('layouts.master')

@section('content')

<div class="page-header">
    <h3>Profile</h3>
</div>

<div class="row">
	<div class="col-sm-3">
		<img src="{{ $account->getAvatarUrl() }}" width="100" height="100" class="profile">
	</div>
	<div class="col-sm-9">
		<p><strong>Email:</strong> {{ $account->email }}</p>
		<p><strong>Name:</strong> {{ $account->fullname }}</p>
		<p><strong>Gender:</strong> @if ($account->gender == 'M') Male @else Female @endif</p>
		<p><strong>Location:</strong> {{ $account->location }}</p>
		<p><strong>Website:</strong> <a href="{{ $account->website }}">{{ $account->website }}</a></p>
	</div>
</div>

<hr>

@if (count($account->posts) > 0)
	@foreach ($account->posts as $post)
		<ul class="list-group">
			<li class="list-group-item">
				<h2><a href="{{ route('post.show', $post->id) }}">{{ $post->title }}</a></h2>
				<small>written on {{ $post->created_at->toFormattedDateString() }} by <a href="{{ route('account.getProfile', $post->user->id) }}">{{ $post->user->fullname }}</a></small>
			</li>
		</ul>
	@endforeach
@endif

@endsection