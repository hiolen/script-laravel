@extends('dashboard.layouts.app')

@section('htmlheader_title')
    Admin Panel
@endsection

@section('contentheader_title')
    Admin Panel
@endsection

@section('header-extra')

@endsection

@section('main-content')
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Dashboard</div>

			<div class="panel-body">
				<a href="{{ route('post.create') }}" class="btn btn-primary btn-xs pull-right">Create post</a>
				<h3>Your Blog Post</h3>

				@if (count($posts) > 0)
				<table class="table table-striped">
					<tr>
						<th>Title</th>
						<th colspan="2" class="text-center">Actions</th>
					</tr>
					@foreach ($posts as $post)
						@if (Auth::user()->id === $post->user->id)
						<tr>
							<td>{{ $post->title }}</td>
							
							<td class="">
								<a href="{{ route('post.edit', $post->id) }}" class="btn btn-default pull-right">Edit</a>
								<form action="{{ route('post.destroy', $post->id) }}" method="POST" class="pull-right">
									{{ csrf_field() }}
									<input type="submit" class="btn btn-danger" value="Delete">
									<input type="hidden" name="_method" value="DELETE">
								</form>
							</td>
						</tr>
						@endif
					@endforeach
				</table>
				@endif
			</div>
		</div>
	</div>
</div>
@endsection
