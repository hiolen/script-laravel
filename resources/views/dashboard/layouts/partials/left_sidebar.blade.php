<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Auth::user()->getAvatarUrl() }}" class="img-circle" alt="User Image"/>
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->firstname." ".Auth::user()->lastname }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
    @endif

    <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
          <span class="input-group-btn">
            <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i
                        class="fa fa-search"></i></button>
          </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">HEADER</li>
        <!-- USE {{ Request::is('route-name*') ? 'active' : '' }} to dynamically set active tab -->
            <li class="{{ request()->path() == "dashboard" ? 'active' : 'n' }}"><a href="{{ route('dashboard') }}"><i
                            class='fa fa-tachometer'></i> <span>Dashboard</span></a></li>
            <li class="{{ request()->path() == "dashboard/post" ? 'active' : 'n' }}"><a href="{{ route('dashboard.post') }}"><i
                            class='fa fa-user'></i> <span>Post</span></a></li>                            
            <li class="{{ Request::is('profile*') ? 'active' : '' }}"><a href="{{ url('profile') }}"><i
                            class='fa fa-user'></i> <span>My Profile</span></a></li>
            <li class="{{ Request::is('admin*') ? 'active' : '' }}"><a href="{{ url('admin') }}"><i
                            class='fa fa-cogs'></i> <span>Admin Panel</span></a></li>
            <li><a href="#"><i class='fa fa-link'></i> <span>Another Link</span></a></li>
            <li class="treeview">
                <a href="#"><i class='fa fa-link'></i> <span>Post</span> <i
                            class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#">Create</a></li>
                    <li><a href="#">Categories</a></li>
                </ul>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
