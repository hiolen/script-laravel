@foreach($posts as $post)
<tr id="{{$post->id}}">
	<td>{{$post->title}}</td>
	<td>{!! str_limit($post->description, 50) !!}</td>
	<td>{{$post->cover_image}}</td>
	<td>{{$post->featured}}</td>
	<td>{{$post->user->fullname}}</td>
	<td>
		<a href="#" class="btn btn-xs btn-info" id="view" data-id="{{$post->id}}">View</a>
		<a href="#" class="btn btn-xs btn-success" id="edit" data-id="{{$post->id}}">Edit</a>
		<a href="#" class="btn btn-xs btn-danger" id="delete" data-id="{{$post->id}}">Delete</a>
	</td>
</tr>
@endforeach