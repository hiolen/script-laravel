<!-- jQuery 2.1.4 -->
<script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset('/js/dashboard/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/js/dashboard/app.min.js') }}" type="text/javascript"></script>
<!-- CKEDITOR -->
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
@yield('scripts-extra')
<!-- Toastr -->
<!-- <script src="{{ asset('/js/dashboard/toastr.min.js') }}" type="text/javascript"></script>
{!! Toastr::render() !!} -->
<!-- SweetAlert2 -->
<script src="{{ asset('/js/dashboard/sweetalert2.min.js') }}" type="text/javascript"></script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
