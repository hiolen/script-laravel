<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">New Post</h4>
      </div>

      <form action="{{ route('dashboard.post-add') }}" method="post" id="form-insert" enctype="multipart/form-data">
        <div class="modal-body">
          
            <div class="form-group">
              <label>Title</label>
              <input type="text" name="title" class="form-control" id="title">
            </div>        
          
            <div class="form-group">
              <label>Description</label>
              <textarea name="description" class="form-control editor" id="addPostCkeditor"></textarea>
            </div>
          
            <div class="form-group">
              <label>Cover Image</label>
              <input type="file" name="cover_image" id="cover_image">
            </div>
          
            <div class="form-group">
              <label>Featured</label>
              <select name="featured" class="form-control" id="featured">
                <option value="0" selected>No</option>
                <option value="1">Yes</option>
              </select>
            </div>
          
            <div class="form-group">
              <label>Author</label>
              <select name="user_id" class="form-control" id="user_id">
                @foreach($users as $key => $value)
                  <option value="{{$key}}">{{$value}}</option>
                @endforeach
              </select>
            </div>
          
        </div>

        <div class="modal-footer">
          <input type="submit" class="btn btn-success pull-left" value="Submit">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>

      </form>
    </div>
    
  </div>
</div>
