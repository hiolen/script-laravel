@extends('dashboard.layouts.app')

@section('htmlheader_title')
    Admin Panel
@endsection

@section('contentheader_title')
    Admin Panel
@endsection

@section('header-extra')

@endsection

@section('main-content')

@include('dashboard.layouts.partials.postAdd')
@include('dashboard.layouts.partials.postEdit')
<div class="panel panel-default">
	<div class="panel-heading">
		<!-- Trigger the modal with a button -->
		<button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal">+ Post</button>

		<button class="btn btn-info pull-right btn-xs" id="read-data">Load Data by Ajax</button>
	</div>

	<div class="panel-body">
		<table class="table table-striped">
			<thead>
				<tr>
					<td>Title</td>
					<td>Description</td>
					<td>Cover Image</td>
					<td>Featured</td>
					<td>Author</td>
					<td>Action</td>
				</tr>
			</thead>
			<tbody id="post-info">
				
			</tbody>
		</table>
	</div>
</div>
@endsection

@section('scripts-extra')
<script type="text/javascript">
	CKEDITOR.replace( 'addPostCkeditor' );
	CKEDITOR.replace( 'editPostCkeditor' );

	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
	});

	// Post list
	$('#read-data').on('click', function(){
		$.get("{{ route('dashboard.post-read') }}", function(data){
			$('#post-info').empty().html(data);
		});
	})

	// Add Post

	$('#form-insert').on('submit', function(e){
		e.preventDefault();

		var extension = $('#cover_image').val().split('.').pop().toLowerCase();

		if ($.inArray(extension, ['jpeg', 'jpg', 'svg', 'png']) == -1) {
	        $('#errormessage').html('Please Select Valid File... ');
	    } else {

	    	var title = $('#title').val();	        
	        var desc = CKEDITOR.instances.addPostCkeditor.getData();
	    	var cover_image = $('#cover_image').prop('files')[0];
	        var featured = $('#featured').val();
	        var user_id = $('#user_id').val();
	        var form_data = new FormData();

	        form_data.append('title', title);
	        form_data.append('description', desc);
	        form_data.append('cover_image', cover_image);
	        form_data.append('featured', featured);
	        form_data.append('user_id', user_id);
	    }

	    var url = $(this).attr('action');
		var post = $(this).attr('method');

	 	$.ajax({
            url: url,
            data: form_data,
            type: post,
            dataType: 'json',
            contentType: false, // The content type used when sending data to the server.
            cache: false, // To unable request pages to be cached
            processData: false,
            success: function(data) {

            	var tr = $('<tr/>', {
            		id: data.id
            	});

            	tr.append($("<td/>", {
            		text: data.title
            	})).append($("<td/>", {
            		text: data.description.substring(0, 50)
            	})).append($("<td/>", {
            		text: data.cover_image
            	})).append($("<td/>", {
            		text: data.featured
            	})).append($("<td/>", {
            		text: data.user_id
            	})).append($("<td/>", {
            		html: '<a href="#" class="btn btn-xs btn-info" id="view" data-id="'+data.id+'">View</a> ' +
            		'<a href="#" class="btn btn-xs btn-success" id="edit" data-id="'+data.id+'">Edit</a> ' + 
            		'<a href="#" class="btn btn-xs btn-danger" id="delete" data-id="'+data.id+'">Delete</a> '
            	}))

            	$('#post-info').append(tr);
            	$('#myModal').modal('hide');
            }
        });
	});

	// Edit Post
	$('body').delegate('#post-info #edit', 'click', function(e){
		var id = $(this).data(id);

		$.get("{{ route('dashboard.post-edit') }}", {id:id}, function(data){
			console.log(data);
			var id = data[0].id
			var title = data[0].title;
			var description = data[0].description;
			var cover_image = data[0].cover_image;
			var featured = data[0].featured;
			var author = data[0].user_id;

			alert(title);

			$('#form-edit').find('#id').val(id);
			$('#form-edit').find('#title').val(title);
			CKEDITOR.instances.editPostCkeditor.setData(description);
			$('#form-edit').find('#featured').val(featured);
			$('#form-edit').find('#author').val(author);

			$('#modal-edit').modal('show');				
		});
	})

	// Update Post
	$('#form-edit').on('submit', function(e){
		e.preventDefault();

		// var extension = $('#cover_image').val().split('.').pop().toLowerCase();

		// if ($.inArray(extension, ['jpeg', 'jpg', 'svg', 'png']) == -1) {
	 //        $('#errormessage').html('Please Select Valid File... ');
	 //    } else {

	 		var id = $('#id').val();
	    	var title = $('#title').val();
	        var desc = CKEDITOR.instances.addPostCkeditor.getData();
	    	// var cover_image = $('#cover_image').prop('files')[0];
	     //    var featured = $('#featured').val();
	     //    var user_id = $('#user_id').val();
	        var form_data = new FormData();

	        alert($('#title').val());

	        form_data.append('id', id);
	        form_data.append('title', title);
	        form_data.append('description', desc);
	    //     form_data.append('cover_image', cover_image);
	    //     form_data.append('featured', featured);
	    //     form_data.append('user_id', user_id);
	    // }

	    var url = $(this).attr('action');
		var post = $(this).attr('method');		

	 	$.ajax({
            url: url,
            data: form_data,
            type: post,
            dataType: 'json',
            contentType: false, // The content type used when sending data to the server.
            cache: false, // To unable request pages to be cached
            processData: false,
            success: function(data) {
            	console.log(data);

            	// var tr = $('<tr/>', {
            	// 	id: data.id
            	// });

            	// tr.append($("<td/>", {
            	// 	text: data.title
            	// })).append($("<td/>", {
            	// 	text: data.description.substring(0, 50)
            	// })).append($("<td/>", {
            	// 	text: data.cover_image
            	// })).append($("<td/>", {
            	// 	text: data.featured
            	// })).append($("<td/>", {
            	// 	text: data.user_id
            	// })).append($("<td/>", {
            	// 	html: '<a href="#" class="btn btn-xs btn-info" id="view" data-id="'+data.id+'">View</a> ' +
            	// 	'<a href="#" class="btn btn-xs btn-success" id="edit" data-id="'+data.id+'">Edit</a> ' + 
            	// 	'<a href="#" class="btn btn-xs btn-danger" id="delete" data-id="'+data.id+'">Delete</a> '
            	// }))

            	// $('#post-info').append(tr);
            	$('#myModal').modal('hide');
            }
        });
	});

	// Delete Post

	$('body').delegate('#post-info #delete', 'click', function(e){
		var id = $(this).data('id');

		$.post("{{route('dashboard.post-destroy')}}", {id:id}, function(data){
			$('tr#'+id).remove();
		});
	});
</script>
@endsection