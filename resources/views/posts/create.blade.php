@extends('layouts.master')

@section('title')
Create Posts
@endsection

@section('content')
<h1>Create Post</h1>
<form action="{{ route('post.store') }}" method="POST" enctype="multipart/form-data">
	{{ csrf_field() }}
  <div class="form-group">
    <input type="text" class="form-control" name="title" id="#" placeholder="Title">
  </div>
  <div class="form-group">
    <textarea class="form-control" id="article-ckeditor" rows="15" name="description" id="#" placeholder="Write something..."></textarea>
  </div>
  <div class="form-group">
  	<input type="file" name="cover_image" >
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>

@endsection