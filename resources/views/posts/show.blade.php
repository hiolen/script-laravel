@extends('layouts.master')

@section('title')
Posts Single Page
@endsection

@section('content')
<a href="{{ route('post.index') }}">Back</a>

<h1>{{ $post->title }}</h1>
<small>Written on {{ $post->created_at->toFormattedDateString() }} by <a href="{{ route('account.getProfile', $post->user->id) }}">{{ $post->user->fullname }}</a></small>
<div>
	<img src="/cover_image/{{$post->cover_image}}" class="img-responsive img-thumbnail">
</div>
<p>{!! $post->description !!}</p>

@if (!Auth::guest())
<hr>

<a href="{{ route('post.edit', $post->id) }}" class="btn btn-default">Edit</a>
<form action="{{ route('post.destroy', $post->id) }}" method="POST" class="pull-right">
	{{ csrf_field() }}
	<input type="hidden" name="_method" value="DELETE">
	<input type="submit" class="btn btn-warning" value="Delete">
</form>
@endif

@endsection