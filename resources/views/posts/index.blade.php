	@extends('layouts.master')

	@section('title')
	Posts
	@endsection

	@section('content')
	@if ($featured)
		<img onclick="window.location='{{route('post.show', $featured->id)}}'" src="/cover_image/{{$featured->cover_image}}" class="img-responsive img-thumbnail" width="100%">
		
		<h2><a href="{{ route('post.show', [$featured->id]) }}">{{ $featured->title }}</a></h2>
		
		<small>written on {{ $featured->created_at->toFormattedDateString() }} by <a href="{{ route('account.getProfile', $featured->user->id) }}">{{ $featured->user->fullname }}</a></small>
		
		<p>{!! str_limit($featured->description, 150) !!}</p>
		
		<button onclick="window.location='{{route('post.show', [$featured->id])}}'" class="btn btn-success">Read more...</button>

		<hr>
	@endif

	@if (count($posts) > 0)	
		<div class="row">
			@foreach ($posts as $post)
			<div class="col-sm-6">
				<img onclick="window.location='{{route('post.show', $post->id)}}'" src="/cover_image/{{$post->cover_image}}" class="img-responsive img-thumbnail" width="100%">

				<h2><a href="{{ route('post.show', [$post->id]) }}">{{ $post->title }}</a></h2>
				
				<small>written on {{ $post->created_at->toFormattedDateString() }} by <a href="{{ route('account.getProfile', $post->user->id) }}">{{ $post->user->fullname }}</a></small>
				
				<p>{!! str_limit($post->description, 150) !!}</p>
				
				<button onclick="window.location='{{route('post.show', [$post->id])}}'" class="btn btn-success">Read more...</button>
			</div>
			@endforeach
		</div>
	{{ $posts->links() }}
	@else
		<p>post not available</p>
	@endif
	@endsection