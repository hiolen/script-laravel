@extends('layouts.master')

@section('title')
Edit Posts
@endsection

@section('content')
<h1>Edit Post</h1>
<form action="{{ route('post.update', $post->id) }}" method="POST" enctype="multipart/form-data">
	{{ csrf_field() }}
  <div class="form-group">
    <input type="text" class="form-control" name="title" id="#" placeholder="Title" value="{{ $post->title }}">
  </div>
  <div class="form-group">
    <textarea class="form-control" id="article-ckeditor" rows="15" name="description" id="#" placeholder="Write something...">{{ $post->description }}</textarea>
  </div>
  <div class="form-group">
    <input type="file" name="cover_image" >
  </div>
  <div class="form-group">
  	<button type="submit" class="btn btn-default">Submit</button>
  </div>
  <input type="hidden" name="_method" value="PUT">
</form>

@endsection