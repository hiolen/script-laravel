@extends('layouts.master')

@section('title')
Home
@endsection

@section('content')
<div class="jumbotron">
	<h1>Script Notes</h1>
	<p>Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
	<p>
		<a href="{{ route('auth.login') }}" class="btn btn-lg btn-primary">Login</a>
		<a href="{{ route('auth.register') }}" class="btn btn-lg btn-success">Rgister</a>
	</p>
</div>
@endsection